package com.apiautomation;

import com.apiautomation.Util.CommonUtil;
import com.apiautomation.exception.ApiAutomationException;
import com.apiautomation.service.comparator_service.ComparatorService;
import com.apiautomation.service.api_calls.RestRequestService;
import com.apiautomation.service.rest_caller.RequestResponse;
import org.apache.log4j.Logger;

import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;


public class AppMain {

    private static Logger logger = Logger.getLogger(AppMain.class);

    static {

        CommonUtil.initializeLog();
    }

    /**
     * @param args  A single entry point for an executable JAR.
     */
    public static void main(String[] args) {

        if (args.length != 2) {

            logger.error("Expecting two input files");

            throw new ApiAutomationException("Expecting two input files");
        }


        BlockingQueue<Map<RequestResponse, RequestResponse>> sharedQueue = new LinkedBlockingQueue<>();

        RestRequestService restRequestService = new RestRequestService(sharedQueue);

        restRequestService.setInputFiles(args[0], args[1]);

        ComparatorService comparatorService = new ComparatorService(sharedQueue);

        // Read data , make rest calls and provide consumer the response for comparing
        Thread restRequests = new Thread(restRequestService, "Rest Request Thread");

        // Consume the producer data compare response and print equals or not equals.
        Thread compareResponse = new Thread(comparatorService, "Compare Response Thread");

        restRequests.start();

        compareResponse.start();
    }
}