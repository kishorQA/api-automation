package com.apiautomation.exception;

public class ApiAutomationException extends RuntimeException {

    /**
     *
     * @param message
     * A wrapper over run time exception class
     */
    public ApiAutomationException(String message){

        super(message);
    }

}
