package com.apiautomation.service.comparator_service;

import com.apiautomation.service.rest_caller.RequestResponse;
import com.apiautomation.service.fileservice.IComparator;
import com.apiautomation.service.fileservice.Wrapper;
import org.apache.log4j.Logger;

import java.io.File;
import java.util.Map;
import java.util.concurrent.BlockingQueue;

/**
 *
 *  This is a consumer thread which will run parallel to producer thread and checks the BlockingQueue for
 *  a Map that contain RequestResponse object sent by producer and then compare the response.
 */
public class ComparatorService implements Runnable, IComparator {

    private Logger logger = Logger.getLogger(ComparatorService.class);

    private BlockingQueue<Map<RequestResponse, RequestResponse>> sharedQueue;

    private static volatile boolean isDone = false;

    public ComparatorService(BlockingQueue<Map<RequestResponse, RequestResponse>> sharedQueue) {

        this.sharedQueue = sharedQueue;
    }

    @Override
    public void run() {

        logger.info("Consumer started");

        try {

            while (!isDone) {

                sharedQueue.take().forEach((k, v) -> {


                    if (k.getResponse().equals("No more Data") && v.getResponse().equals("No more Data")) {

                        logger.info("No more data to be processed");

                        this.stop();

                        return;

                    }

                    if (compare(k, v)) {

                        logger.info("Response matched");

                        System.out.println(k.getRequest() + " equals " + v.getRequest());

                    } else {

                        logger.info("Response not matched");

                        System.out.println(k.getRequest() + " Not equals " + v.getRequest());
                    }

                });
            }


        } catch (InterruptedException ex) {

            logger.error("Consumer thread interrupted" + ex.getMessage());

            this.stop();
        }

    }

    public void stop() {

        logger.info("Stopping consumer thread");

        isDone = true;

        logger.info("Consumer finished");
    }

    @Override
    public boolean compare(Object o, Object o2) {

        if (o instanceof RequestResponse && o2 instanceof RequestResponse) {

            return ((RequestResponse) o).getResponse().equals(((RequestResponse) o2).getResponse());
        }

        return false;
    }

    @Override
    public Wrapper getData(File file1, File file2) {
        return null;
    }
}
