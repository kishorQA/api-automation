package com.apiautomation.service.fileservice;

import java.io.File;

public interface IComparator <X, Y> {

    boolean compare(X x, Y y) ;

    Wrapper<X, Y> getData(File file1, File file2);

}