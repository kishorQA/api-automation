package com.apiautomation.service.fileservice;

import lombok.Data;

import java.util.List;

@Data
public class SecondFile {

    private List<String> secondFileLines;
}
