package com.apiautomation.service.fileservice;

import lombok.Data;

import java.util.List;

@Data
public class FirstFile {

   private List<String> firstFileLines;
}
