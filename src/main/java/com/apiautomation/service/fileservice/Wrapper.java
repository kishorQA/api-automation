package com.apiautomation.service.fileservice;

import com.apiautomation.exception.ApiAutomationException;
import lombok.Getter;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Getter
public class Wrapper<X, Y> implements IComparator {

    private Logger logger = Logger.getLogger(Wrapper.class);

    private X file1;

    private Y file2;


    public Wrapper(X x, Y y) {

        this.file1 = x;
        this.file2 = y;
    }


    @Override
    public boolean compare(Object object1, Object object2) {

        return object1.equals(object2);
    }

    /**
     * @param file1
     * @param file2
     * @return This function takes two file parameter , read them and store the content in memory for further processing
     */

    @Override
    public Wrapper<X, Y> getData(File file1, File file2) {

        if(file1.isFile() && file2.isFile()) {

            try {

                if (this.file1 instanceof FirstFile) {

                    logger.info("Starts reading from " + file1.getName());

                    System.out.println("Reading files . Wait ...");

                    ((FirstFile) this.file1).setFirstFileLines(

                            Files.readAllLines(Paths.get(file1.getPath()))
                    );

                    logger.info("read done from first file " + file1.getName());

                    System.out.println("read done from first file");

                }

                if (this.file2 instanceof SecondFile) {

                    logger.info("Starts reading from " + file2.getName());

                    System.out.println("Reading files . Wait ...");

                    ((SecondFile) this.file2).setSecondFileLines(

                            Files.readAllLines(Paths.get(file2.getPath()))
                    );

                    logger.info("read done from second file " + file1.getName());

                    System.out.println("read done from second file");
                }

            } catch (IOException e) {

                logger.error("IO Exception  in reading file" + e.getMessage());

                System.exit(0);
            }
        }else {

            throw new ApiAutomationException("Provide a valid file");
        }
        return this;
    }
}
