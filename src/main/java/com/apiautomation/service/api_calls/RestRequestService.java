package com.apiautomation.service.api_calls;

import com.apiautomation.exception.ApiAutomationException;
import com.apiautomation.service.rest_caller.RequestResponse;
import com.apiautomation.service.rest_caller.RestCaller;
import com.apiautomation.service.fileservice.FirstFile;
import com.apiautomation.service.fileservice.SecondFile;
import com.apiautomation.service.fileservice.Wrapper;
import org.apache.log4j.Logger;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 *  This a producer class . It will call Wrapper to read data from files which we have to provide as input.
 *  These file lines are used by Rest Caller to make API requests.
 *  Request URL and Response are wrapped in RequestResponse  object and then sent
 *  to a BlockingQueue for consumer to process(compare response)
 *  BlockingQueue is of type Map<RequestResponse, RequestResponse> means key is the RequestResponse of urls for file1
 *  and value is the RequestResponse of urls for file1
 *
 */
public class RestRequestService implements Runnable {

    private Logger logger = Logger.getLogger(RestRequestService.class);

    private final BlockingQueue<Map<RequestResponse, RequestResponse>> sharedQueue;

    private Wrapper<FirstFile, SecondFile> wrapper = new Wrapper<>(new FirstFile(), new SecondFile());

    private static volatile boolean isDone = false;

    private File file1;

    private File file2;


    public void setInputFiles(String file1, String file2) {

        this.file1 = new File(file1);

        this.file2 = new File(file2);
    }

    public RestRequestService(BlockingQueue<Map<RequestResponse, RequestResponse>> sharedQueue) {

        this.sharedQueue = sharedQueue;
    }


    @Override
    public void run() {

        int count = 0;

        wrapper = wrapper.getData(file1, file2);

        Map<RequestResponse, RequestResponse> map = new ConcurrentHashMap<>();

        List<String> firstSetOfURLs = wrapper.getFile1().getFirstFileLines();

        List<String> secondSetOfURLs = wrapper.getFile2().getSecondFileLines();

        if (firstSetOfURLs != null && secondSetOfURLs != null) {

            if (firstSetOfURLs.size() != secondSetOfURLs.size()) {

                logger.error("Expected same no URLS in both files to compare response");

                throw new ApiAutomationException("Expected same no URLS in both files to compare response");
            }
        }

        try {

            while (!isDone && count < firstSetOfURLs.size()) {

                logger.info("producer starts producing  data");

                logger.info("reading data from file1 and file2");

                RequestResponse o1 = new RestCaller().makeRestCalls(firstSetOfURLs.get(count));

                RequestResponse o2 = new RestCaller().makeRestCalls(secondSetOfURLs.get(count));

                count++;

                map.clear();

                map.put(o1, o2);

                logger.info("Sending response for comparision");

                sharedQueue.put(map);
            }

            if (count == firstSetOfURLs.size() && count == secondSetOfURLs.size()) {

                logger.info("all URL processed");

                logger.info("stopping the thread");

                sharedQueue.put(stopConsumer());

                this.stop();
            }
        } catch (InterruptedException ex) {

            logger.error("producer thread interrupted" + ex.getMessage());

            this.stop();
        }

    }


    public void stop() {

        logger.info("Stopping producer thread");

        isDone = true;

        logger.info("producer finished");
    }

    /**
     *
     * @return used to notify consumer that there are no more data to compare and consumer can stop
     */
    private static Map<RequestResponse, RequestResponse> stopConsumer() {

        Map<RequestResponse, RequestResponse> poison = new ConcurrentHashMap<>();

        RequestResponse r1 = new RequestResponse();

        r1.setResponse("No more Data");

        poison.put(r1, r1);

        return poison;
    }


}
