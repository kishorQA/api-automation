package com.apiautomation.service.rest_caller;

import lombok.Data;

@Data
public class RequestResponse {

    private String request;

    private String  response;
}
