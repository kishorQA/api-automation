package com.apiautomation.service.rest_caller;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.log4j.Logger;

import java.net.MalformedURLException;
import java.net.URL;

public final class RestCaller {

    private Logger logger = Logger.getLogger(RestCaller.class);

    public RequestResponse makeRestCalls(String url) {

        return new MakeRequests(url).call();
    }


    /**
     * Make Rest Calls . A wrapper over Rest Assured Library.
     */
    private class MakeRequests {

        private String url;

        private RequestSpecification specification = RestAssured
                .with()
                .accept(ContentType.JSON)
                .log()
                .all(true);

        private Response response;

        private RequestResponse requestResponse = new RequestResponse();

        private MakeRequests(String url) {

            this.url = url;

        }

        private synchronized RequestResponse call() {

            try {

                logger.info("Making GET rest call to " + url);

                response = specification.get(new URL(url));

                logger.info("Got response from service");

                requestResponse.setRequest(url);

                requestResponse.setResponse(response.asString());

                return requestResponse;

            } catch (MalformedURLException ex) {

                logger.error("MalformedURLException on " + url);
            }

            return requestResponse;
        }
    }
}


