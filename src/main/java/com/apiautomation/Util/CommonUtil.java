package com.apiautomation.Util;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;


public class CommonUtil {

    private static Logger logger = Logger.getLogger(CommonUtil.class);

    public static void initializeLog(){

        PropertyConfigurator.configure("log4j.properties");
    }
}
