package tests;

import com.apiautomation.Util.CommonUtil;
import com.apiautomation.service.api_calls.RestRequestService;
import com.apiautomation.service.comparator_service.ComparatorService;
import com.apiautomation.service.fileservice.FirstFile;
import com.apiautomation.service.fileservice.SecondFile;
import com.apiautomation.service.fileservice.Wrapper;
import com.apiautomation.service.rest_caller.RequestResponse;
import org.testng.annotations.BeforeSuite;

import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class TestBase {

    RestRequestService restRequestService;

    ComparatorService comparatorService;

    Wrapper<FirstFile, SecondFile> wrapper;


    @BeforeSuite
    public void setUp() {

        CommonUtil.initializeLog();

        wrapper = new Wrapper<>(new FirstFile(), new SecondFile());

        BlockingQueue<Map<RequestResponse, RequestResponse>> sharedQueue = new LinkedBlockingQueue<>();

        restRequestService = new RestRequestService(sharedQueue);

        comparatorService = new ComparatorService(sharedQueue);

    }

}
