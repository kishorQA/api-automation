package tests;


import com.apiautomation.exception.ApiAutomationException;
import com.apiautomation.service.rest_caller.RequestResponse;
import com.apiautomation.service.rest_caller.RestCaller;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.File;
import java.util.stream.IntStream;

public class UnitTests extends TestBase {


    @Test(expectedExceptions = ApiAutomationException.class)
    public void getData_Negative1() {

        wrapper.getData(new File("test1"), new File("test2"));

    }

    @Test()
    public void getData_Positive1() {

        wrapper = wrapper.getData(new File("src/test/resources/dataset1/input File 1.txt"), new File("src/test/resources/dataset1/Input File 2.txt"));

        Assert.assertNotNull(wrapper);

    }

    @Test()
    public void getFileContent() {

        wrapper = wrapper.getData(new File("src/test/resources/dataset1/input File 1.txt"), new File("src/test/resources/dataset1/Input File 2.txt"));

        Assert.assertNotNull(wrapper.getFile1().getFirstFileLines());

        Assert.assertNotNull(wrapper.getFile2().getSecondFileLines());

    }

    @Test()
    public void makeRestCalls_Same_Response() {

        wrapper = wrapper.getData(new File("src/test/resources/dataset1/input File 1.txt"), new File("src/test/resources/dataset1/Input File 2.txt"));

        IntStream.range(0, 1).forEach(i -> {

            String url1 = wrapper.getFile1().getFirstFileLines().get(i);

            String url2 = wrapper.getFile2().getSecondFileLines().get(i);

            RequestResponse r1 = new RestCaller().makeRestCalls(url1);

            RequestResponse r2 = new RestCaller().makeRestCalls(url2);

            Assert.assertEquals(r1.getResponse(), r2.getResponse());
        });
    }


    @Test()
    public void makeRestCalls_Different_Response() {

        wrapper = wrapper.getData(new File("src/test/resources/dataset2/input File 1.txt"), new File("src/test/resources/dataset2/Input File 2.txt"));

        IntStream.range(0, 1).forEach(i -> {

            String url1 = wrapper.getFile1().getFirstFileLines().get(i);

            String url2 = wrapper.getFile2().getSecondFileLines().get(i);

            RequestResponse r1 = new RestCaller().makeRestCalls(url1);

            RequestResponse r2 = new RestCaller().makeRestCalls(url2);

            Assert.assertNotEquals(r1.getResponse(), r2.getResponse());
        });

    }

    @Test()
    public void makeRestCalls_Invalid_URL() {

        wrapper = wrapper.getData(new File("src/test/resources/dataset3/input File 1.txt"), new File("src/test/resources/dataset3/Input File 2.txt"));

        IntStream.range(0, 1).forEach(i -> {

            String url1 = wrapper.getFile1().getFirstFileLines().get(i);

            String url2 = wrapper.getFile2().getSecondFileLines().get(i);

            RequestResponse r1 = new RestCaller().makeRestCalls(url1);

            RequestResponse r2 = new RestCaller().makeRestCalls(url2);

            Assert.assertTrue(r1.getResponse() == null && r2.getResponse() == null);

        });

    }


    @Test()
    public void makeRestCallsAndCompare() {

        restRequestService.setInputFiles("src/test/resources/dataset1/input File 1.txt", "src/test/resources/dataset1/Input File 2.txt");

        restRequestService.run();

        comparatorService.run();

    }

}

