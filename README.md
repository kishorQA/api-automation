####  API Automation
This is a comparator library that can be used to compare 2 API responses (HTTP/HTTPS).
The input to the program are two files with request URLs.
This is a multi threaded program that can make Rest calls to request urls of
file1 and file2 (input files) and compare the response.

#### HOW TO RUN ? 

    Assuming git is already installed in the system
    
    Clone the projet : https://gitlab.com/kishorQA/api-automation.git
    
    Navigate to the  project root directory run : mvn package 
    
    This will produce an executable JAR . 
    
    we can execute the JAR as : java -jar {jar file -name} "arg1" "arg2"
    
    app logs will be written in app.log in current project directory.
    

    
  


 

